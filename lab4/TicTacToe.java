import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int free = 3 * 3;
		while (free > 0) {
			printBoard(board);
			while (true) {
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (isFree(row, col, board)) {
					board[row - 1][col - 1] = 'X';
					printBoard(board);
					break;
				} else
					System.out.println("Please enter a valid empty coordinate.");
			}
			if (isWon(board)) {
				System.out.println("Player 1 won!");
				return;
			}
			while (true) {
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (isFree(row, col, board)) {
					board[row - 1][col - 1] = 'O';
					printBoard(board);
					break;
				} else
					System.out.println("Please enter a valid empty coordinate.");
			}
			if (isWon(board)) {
				System.out.println("Player 2 won!");
				return;
			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}
	}

	public static boolean isFree(int row, int col, char[][] board) {
		if (row > 3 || col > 3)
			return false;
		if (board[row - 1][col - 1] != ' ')
			return false;
		return true;
	}

	public static boolean isWon(char[][] board) {
		if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ' && board[1][1] != ' '
				&& board[2][2] != ' ') // type: \
			return true;
		if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ' && board[1][1] != ' '
				&& board[2][0] != ' ') // type: /
			return true; 	
		if (board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] != ' ' && board[0][1] != ' '
				&& board[0][0] != ' ') // type: - top
			return true;
		if (board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] != ' ' && board[1][1] != ' '
				&& board[1][2] != ' ') // type: - middle
			return true;
		if (board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] != ' ' && board[2][1] != ' '
				&& board[2][2] != ' ') // type: - bottom
			return true;
		if (board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] != ' ' && board[1][0] != ' '
				&& board[2][0] != ' ') // type: | left
			return true;
		if (board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] != ' ' && board[1][1] != ' '
				&& board[2][1] != ' ') // type: | center
			return true;
		if (board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] != ' ' && board[1][2] != ' '
				&& board[2][2] != ' ') // type: | right
			return true;

		return false;
	}

}
