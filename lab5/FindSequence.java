import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FindSequence {

    public static void main(String[] args) throws FileNotFoundException {
        int matrix[][] = readMatrix();

        boolean found = false;
        search: for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (search(new ArrayList<Coords>(), 0, matrix, i, j)) {
                    found = true;
                    break search;
                }
            }
        }

        if (found) {

            System.out.println("A sequence is found");
        }
        printMatrix(matrix);

    }

    static class Coords {
        int x, y;

        Coords(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private static boolean search(ArrayList<Coords> sequence, int number, int[][] matrix, int row, int col) {
        if (number == 10) {
            reverseMatrix(sequence, matrix);
            return true;
        }
        if (row < 0 || col < 0 || row >= matrix.length || col >= matrix[row].length)
            return false;
        // System.out.println(number+": "+row+","+col +" = "+ matrix[row][col]);
        if (matrix[row][col] == number) {
            sequence.add(new Coords(row, col));
            return (search(sequence, number + 1, matrix, row - 1, col)
                    || search(sequence, number + 1, matrix, row + 1, col)
                    || search(sequence, number + 1, matrix, row, col - 1)
                    || search(sequence, number + 1, matrix, row, col + 1));
        }
        return false;
    }

    private static void reverseMatrix(ArrayList<Coords> seq, int[][] matrix) {
        for (Coords i : seq) {
            matrix[i.x][i.y] = 9 - matrix[i.x][i.y];
        }
    }

    private static int[][] readMatrix() throws FileNotFoundException {
        int[][] matrix = new int[10][10];
        File file = new File("lab5/matrix.txt"); // if you run from command line use new File("matrix.txt") instead

        try (Scanner sc = new Scanner(file)) {

            int i = 0;
            int j = 0;
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                matrix[i][j] = number;
                if (j == 9)
                    i++;
                j = (j + 1) % 10;
                if (i == 10)
                    break;
            }
        } catch (FileNotFoundException e) {
            throw e;
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
