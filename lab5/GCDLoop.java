public class GCDLoop{
    public static void main(String[] args) {
        if(args.length < 2){
            System.out.println("Enter 2 valid numbers");
            return;
        }
        int a=Integer.valueOf(args[0]);
        int b=Integer.valueOf(args[1]);
        if(b>a){
            int temp=a;
            a=b;
            b=temp;
        }

        while(a%b!=0){
            int temp=b;
            b=a%b;
            a=temp;
        }
        System.out.println(b);

    }
}