public class GCDRec{
    public static void main(String[] args) {
        if(args.length < 2){
            System.out.println("Enter 2 valid numbers");
            return;
        }
        int a=Integer.valueOf(args[0]);
        int b=Integer.valueOf(args[1]);
        if(b>a){
            int temp=a;
            a=b;
            b=temp;
        }
        findGCD(a, b);
    }

    public static void findGCD(int a, int b){

        if(a%b == 0)
            System.out.println(b);
        else{
            int temp=b;
            b=a%b;
            findGCD(temp, b);
        }
    }
}