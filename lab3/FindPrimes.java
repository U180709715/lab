class FindPrimes{
    public static void main(String[] args){
        if(args.length < 1){
            System.out.println("Please enter a number!");
            return;
        }

        int num=Integer.valueOf(args[0]);
        if(num<2){
            System.out.println("No prime numbers found!");
            return;
        }
        String ans="";
        if(num>=2)
            ans="2,";

        for(int i=3; i<=num; i=i+2){
            if(isPrime(i))
                ans+=i+",";
        }
        System.out.println(ans.substring(0, ans.length()-1));
    }
    public static boolean isPrime(int n){
        if(n==1)
            return false;
        boolean prime=true;
        for(int i=2; i<n; i++){
            if(n % i==0)
            prime=false;
        }
        return prime;
    }
}