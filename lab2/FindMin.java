public class FindMin {

	public static void main(String[] args){

                if(args.length==0){
                        System.out.println("You should add args.");
                        return;
                }

                int min=Integer.parseInt(args[0]);
                if(args.length==1){
                        System.out.println(min);
                        return;
                }

                for(int i=1; i<args.length; i++){
                        int num=Integer.parseInt(args[i]);
                        if(num < min)
                                min=num;
                }
                System.out.println(min);
	}
}