public class FindGrade {

	public static void main(String[] args){
        if(args.length==0){
                System.out.println("You should add args.");
                return;
        }

        int num=Integer.parseInt(args[0]);
        if(num > 100 || num < 0){
            System.out.println("This is not a valid grade out of 100");
            return;
        }

        if (num >= 90)
            System.out.println("A");
        else if (num >= 80)
            System.out.println("B");
        else if (num >= 70)
            System.out.println("C");
        else if (num >= 60)
            System.out.println("D");
        else
            System.out.println("F");
	}
}