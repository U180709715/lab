public class MainClass {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 5, new Point(0, 5));
        System.out.println("Rectangle:");
        System.out.println("Area: " + rectangle.area() + "\nPerimeter: " + rectangle.perimeter());
        System.out.println("Points:");
        for (Point p : rectangle.corners()) {
            System.out.println("(" + p.xCoord + "," + p.yCoord + ")");
        }
        System.out.println("-----------------------------\nCircle:");
        Circle circle1 = new Circle();
        circle1.radius = 10;
        System.out.println("Area: " + circle1.area());
        System.out.println("Perimeter: " + circle1.perimeter());
        circle1.center = new Point(5, 5);
        Circle circle2 = new Circle();
        circle2.radius = 5;
        circle2.center = new Point(9, 9);
        System.out.println("Intersect: " + circle1.intersect(circle2));
    }
}
