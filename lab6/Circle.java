class Circle {
    int radius;
    Point center;

    int area() {
        return (int) Math.round(Math.PI * radius * radius);
    }

    int perimeter() {
        return (int) Math.round(2 * Math.PI * radius);
    }

    boolean intersect(Circle circle) {
        double c = Math.sqrt(Math.pow(center.xCoord - circle.center.xCoord, 2) + Math.pow(center.yCoord - circle.center.yCoord, 2));
        return (Math.abs(radius - circle.radius) <= c && c <= radius + circle.radius);
    }
}
