class Rectangle {
    private int sideA, sideB;
    private Point topLeft;

    Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    int area() {
        return sideA * sideB;
    }

    int perimeter() {
        return (sideA * 2) + (sideB * 2);
    }

    Point[] corners() {
        Point topRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord);
        Point bottomLeft = new Point(topLeft.xCoord, topLeft.yCoord - sideB);
        Point bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord - sideB);
        return new Point[]{topLeft, topRight, bottomLeft, bottomRight};
    }
}
